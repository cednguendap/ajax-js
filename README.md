# AJAX JS
[![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](https://forthebadge.com)


Ce projet facilite l'utilisation d'Ajax, permet l'utilisation de façon simple et est open-souce




## Pré-requis
Aucun programme particulier n'est demandé.Ce programme est écris en JavaScript natif

## Installation
Aucune installation particulière n'est demandé. vous devez juste télécharger le projet et il est prêt pour l'utilisation 

## Documentation
Pour utilisé vous pouvez le faire de 2 façon:
* pour une requette post par example appelez la fonction `r_post.post(url,data,callback,callback_progress)` :
	* `url` represente l'url de votre serveur distant
	* `data` represente les donnés que vous voulez envoyé. il peuvent être sous format JSON, être un table de valeur, une chaine de charractère, un entier, un reel ou un objet recupere avec la fonction `document.getElementById()` ou `document.querySelector()`
	* `callback` représente la fonction de callback appelé lorsque la requete est terminée. la fonction doit prendre en parametre un objet qui peut être un JSON, un type de base (entier,reel,chaine de charractere), un objet au format XML.Tout dépend du contenu de l'entête `Content-Type` de la reponse a la requête.
	* `callback_progress` est la fonction qui est appelé pour une affichage de la progression de la requette. Cette fonction n'est pas obligatoire et peut-être ignoré.
Pour une requete de type get veuillez juste remplacez `r_post` par `r_get`, la methode `post` par la methode `get` et le comportement est le même

* La seconde methode est d'instancier la classe `r_post` un example suit:

        let req = new r_post("url_vers_mon_serveur", "login_eventuelle_du_serveur", "mot_de_passe_eventuelle_du_serveur");
        //Si votre srveur ne requiere pas de login et de mot de passe vous pouvez ignorer ces 2 arguments
        req.run({ "obj1" : 1, "obj2" : "valeur2" }, document.querySelector("id_formulaire"));
        req.send(function(result)
        {
            console.log(result);//affichage du resultat
        });

**Une documentation plus compléte seras fournis d'ici peu!**

# Contribution

Si vous souhaitez contribuer, lisez le fichier CONTRIBUTING.md pour savoir comment le faire.

# Version

Les différentes versions sont:
* v2.0 respecte l'ECMAScritp 2015 
* v1.1 version amélioré de la version initiale
* v1.0 version initiale

# Auteurs

Liste des auteurs
* Cedric Nguendap alias @cednguendap , mail: cednguenda@gmail.com

# Licence

Cette projet est distribué sous licence MIT. vous pouvez donc l'intégrer dans vos projets, le modifier et le redistribuer selon les termes de la licence sous laquel il est soumit.
