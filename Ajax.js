

										/////////////////////////////////////////////////////////////////////////////////////////
										//   ________________________________________________________________________________  //
										//   |                                                                              |  //
										//   |        @Auteur: Nguendap Bedjama Cedric                                      |  //
										//   |        @Date de creation: 10/6/2018                                          |  //
										//   |        @Date de derniere modification: 25/07/2019                            |  //
										//   |        @email: cednguendap@gmail.com                                         |  //
										//   |        @version: 2.0                                                         |  //
										//   |______________________________________________________________________________|  //
										//                                                                                     //
										/////////////////////////////////////////////////////////////////////////////////////////






class Requete
{
	"use strict"
	constructor(UrlServeur,loginServeur,MdpServeur,modeServeur)
	{
		//Doit verifier si sa connait la requette de type XmlHttpRquest
		if(window.XMLHttpRequest || window.ActiveXObject)
		{
			if(window.ActiveXObject)
			{
				try{
					this.__xhr=new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e){
					this._xhr=new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			else
			{
				this._xhr=new XMLHttpRequest();
			}
			this._serveur=UrlServeur;
			this._login=loginServeur || "";
			this._password=MdpServeur || "";
			this._mode=modeServeur || true ;
			this._timeout=1000;
			this._xhr=this.timeout;
			this._responseType='json';
		}
		else
		{
			this._xhr=null;
			//doit lancer une exception du a la non aceptation de l'object
			//_xhr
		}
	}
	exitRequest()
	{
		this._xhr.abort();
	}
	status()
	{
		return this._xhr.status;
	}
	set	timeout(time)
	{
		if(typeof time!="number") throw new Error("le temps doit être un entier naturel");
		this._timeout=time;
		this._xhr.timeout=time;
	}
	Header(elmt)
	{
		return this._xhr.getResponseHeader(elmt);
	}
	setResponseType(type)
	{
		this._responseType=type;
		this._xhr.overrideMimeType('text/'+type);
	}
	withCredentials(value)
	{
		if(value)
		{
			if(typeof this._xhr.withCredentials!='undefined')
			{
				this._xhr.withCredentials=true;
			}
			else
			{
				//ajout d'une entete HTTP permettant d'envoyer les cookies
				//Probablement credentials
			}
		}
	}
	setHeader()
	{
		if(arguments.length==0 || (arguments.length==2 && (typeof arguments[0]!="string" || typeof arguments[1]!="string") ) || (arguments.length==1 && typeof arguments[0]!="object")) throw Error("InvalidArgumentException setHeader Ajax function");
		else
		{
			if (arguments.length==1)
			{
				for(let $key in arguments[0])
				{
					this._xhr.setRequestHeader($key,arguments[0][$key]);	
				}
			}
			else
			{
				$this._xhr.setRequestHeader(arguments[0],arguments[1]);
			}
		}
	}
	get xhr()
	{
		return this._xhr;
	}
	abort()
	{
		this._xhr.abort();
	}
	onabort(callback)
	{
		this._xhr.addEventListener('abort',callback,false);
	}
	ontimeout(callback,time)
	{
		if(arguments.length==2)
		{
			this.timeout=time;
		}
		this._xhr.addEventListener('timeout',callback,false);
	}
	onerror(callback)
	{
		this._xhr.addEventListener('error',callback,false);
	}
	onreceiv(callBackFunction)
	{
		if(this._mode==true)
		{
			this._xhr.onreadystatechange=function(e)
			{
				let _xhr=e.currentTarget;
				if(_xhr.readyState==_xhr.DONE && _xhr.status==200)
				{ 

					let rps_type=e.currentTarget.getResponseHeader('Content-Type');
					if(rps_type.indexOf('xml')>-1)
					{
						callBackFunction(_xhr.responseXML);
					}
					else if(rps_type.indexOf('json')>-1)
					{
						//console.log(_xhr.responseText);
						callBackFunction(JSON.parse(_xhr.responseText));
					}
					else
					{
						callBackFunction(_xhr.responseText);
					}
				}
			};
		}
		else
		{
			let rps_type=this.Header('Content-Type');
			if(rps_type.indexOf('XML')>-1)
			{
				callBackFunction(this._xhr.responseXML);
			}
			else if(rps_type.indexOf('JSON')>-1)
			{
				callBackFunction(JSON.parse(this._xhr.responseText));
			}
			else
			{
				callBackFunction(this._xhr.responseText);
			}	
		}
	}
	get allHeader()
	{
		return this._xhr.getAllResponseHeaders();
	}
	setJsonHeader()
	{
		this.setHeader({"Content-Type":"application/json"});
	}
	onloadend(callback)
	{
		this._xhr.addEventListener('loadend',callback,false);
	}
	onprogress(functionParam)
	{
		this._xhr.onprogress=function(e)
		{
			functionParam(e.loaded,e.total);
		}
	}
}


class r_post extends Requete
{
	run($keys)
	{
		let args=$keys || "argv";
		this._xhr.open('POST',this.serveur,this.mode,this.login,this.password);
		this._method='POST';
		if(arguments.length>0 && typeof arguments[0].nodeName!='undefined' && arguments[0].nodeName.toLowerCase()=='form')
		{
			this.Oform=new FormData(arguments[0]);
		}
		else
		{
			this.Oform=new FormData();
			this.Oform.append(args,JSON.stringify(arguments[0]));
		}
	}
	append()
	{
		if(arguments.length==0 || (arguments.length==2 && (typeof arguments[0]!="string" || typeof arguments[1]!="string") ) || (arguments.length==1 && typeof arguments[0]!="object")) throw Error("InvalidArgumentException setHeader Ajax function");
		else
		{
			if (arguments.length==1)
			{
				for(let key in arguments[0])
				{
					this.Oform.append(key,arguments[0][key]);
				}
			}
			else if(arguments.length==2)
			{
				this.Oform.append(arguments[0],arguments[1]);
			}
			else
			{
				this.Oform.append(arguments[0],arguments[1],arguments[2]);
			}
		}
	}
	remove()
	{
		for(let i=0;i<arguments.length;i++)
		{
			this.Oform.delete(arguments[i]);
		}
	}
	set()
	{
		if(this._xhr.has(arguments[0]))
		{
			this.remove(arguments[0]);
			this.append(arguments);
		}
	}
	send(callBackFunction)
	{
		//Pour le post	
		this.setHeader({'Content-Type','application/x-www-form-urlencoded'});
		this._xhr.send(this.Oform);
		this.onreceiv(callBackFunction);
	}
	static post(url,data,callback,callback_progress)
	{
		let r=new post(url);
		r.run(data);
		if(arguments.length==4) r.onprogress(callback_progress);
		r.send(callback);
	}
}

class r_get extends Requete
{

	static get(url,data,callback,callback_progress)
	{
		let g=new get(url,data,callback,callback_progress);
		g.run(data);
		if(arguments.length==4) g.onprogress(callback_progress);
		g.send(callback);
	}
	run(Param) 
	{
		this._method='GET';
		let ListeParam='';
		if(Param instanceof Array)
		{
			for (let j = 0; j < Param.length-1; i++) {
				ListeParam=ListeParam+j+'='+encodeURIComponent(Param[j])+'&';
			}
			ListeParam=ListeParam+encodeURIComponent(Param[j+1]);
		}
		else if(typeof(Param)=='object')
		{
			for(let elmt in Param)
			{
				ListeParam=ListeParam+elmt+'='+encodeURIComponent(Param[elmt])+'&';
			}
			ListeParam=ListeParam.substr(0,ListeParam.lastIndexOf('&'));
		}
		else
		{
			ListeParam=j+'='+encodeURIComponent(Param);
		}
		this._xhr.open('GET',this._serveur+'?'+ListeParam,this._mode,this._login,this._password);
	}
	send(callBackFunction)
	{
		this._xhr.send(null);
		this.onreceiv(callBackFunction);
	}
}